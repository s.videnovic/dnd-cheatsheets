#!/usr/bin/bash

sudo docker build -t dnd_cheatsheets .
echo ""

#sudo docker run -it dnd_cheatsheets bash

# TODO: figure out how to give regular permissions to character files when creating them

sudo docker run \
    -v /home/vidzor/Projects/dnd_cheatsheets/data:/home/dnd/dnd_cheatsheets/data -it \
    dnd_cheatsheets python3 -m dnd_cheatsheets.main --char character_Fila.cah --init

sudo docker run \
    -v /home/vidzor/Projects/dnd_cheatsheets/data:/home/dnd/dnd_cheatsheets/data -it \
    dnd_cheatsheets python3 -m dnd_cheatsheets.main --char character_Fila.cah --gen-cs
