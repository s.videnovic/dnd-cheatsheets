
from contextlib import contextmanager
import sys, os

@contextmanager
def suppress_stdout():
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:
            yield
        finally:
            sys.stdout = old_stdout


def printProgressBar( iteration, total, prefix='', suffix='', decimals=1, length=100, fill='=' ):
	"""
	Call in a loop to create terminal progress bar
	@params:
		iteration   - Required  : current iteration (Int)
		total       - Required  : total iterations (Int)
		prefix      - Optional  : prefix string (Str)
		suffix      - Optional  : suffix string (Str)
		decimals    - Optional  : positive number of decimals in percent complete (Int)
		length      - Optional  : character length of bar (Int)
		fill        - Optional  : bar fill character (Str)
	Example usage:
		for i, img in enumerate(img_files):
			printProgressBar(
				i+1, len(img_files), prefix = f'Loading Data: {i+1}/{len(img_files)}', length = 50
			)
	"""
	percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
	filledLength = int(length * iteration // total)
	bar = fill * filledLength + '.' * (length - filledLength)
	os.sys.stdout.write('\r %s |%s| %s%% %s' % (prefix, bar, percent, suffix))
	os.sys.stdout.flush()
	# if iteration == total: os.sys.stdout.write('\n')