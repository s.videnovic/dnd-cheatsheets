
import io
import json
import numpy as np
from collections import OrderedDict
import imgkit
from PIL import Image
from dnd_cheatsheets.utils import suppress_stdout

config = dict(
	page_size_px = [2480, 3508],  # A4 format in pixels
	page_size_cm = [21, 29.7],  # A4 format in cm
	outer_frame_cm = [19.5, 25.6],
	inner_frame_cm = [14, 20.3],
	border_thickness = 2,
	slot_side = 190,
	printing_correction = 1.05  # enlarge to account for printing margins
)

class SlotsUtils:
	@staticmethod
	def init_slots_sliders():
		# TODO: get "spellSlots" from char.json and init slots.json
		pass

	@staticmethod
	def load_slots(slots_src_path):
		with open(slots_src_path, "r") as slots_fp:
			slots = OrderedDict(json.load(slots_fp))
		# flatten/expand dict:
		slots_exp = OrderedDict({})
		for key, val in slots.items():
			if key == "spell_slots":
				for i, ss in enumerate(val):
					slots_exp[f"ss-{i + 1}"] = ss
			else:
				slots_exp[key] = val
		return slots_exp

	@staticmethod
	def render_to_html(slots):
		page_size = np.array(config["page_size_px"])
		page_size_cm = np.array(config["page_size_cm"])
		outer_frame_cm = np.array(config["outer_frame_cm"])
		inner_frame_cm = np.array(config["inner_frame_cm"])
		border_radius = int(np.mean(
			page_size * (outer_frame_cm - inner_frame_cm) / (2 * page_size_cm))
		)
		outer_frame = outer_frame_cm / page_size_cm
		outer_frame_size = (page_size * outer_frame).astype(int)
		border_thickness = config["border_thickness"]
		slot_side = config["slot_side"]
		printing_correction = config["printing_correction"]

		html = ""
		for slot_name, num_slots in slots.items():
			slot_divs_array = ""
			slot_div_css_style = "".join([
				"float: left;",
				f"width: {slot_side - border_thickness}px;",
				f"height: {slot_side}px;",
				f"border-right: {border_thickness}px solid black;"
			])
			slot_num_css_style = "".join([
				"float: left;",
				"font-size: 42px;",
				"font-family: Arial;",
				"padding: 0 0 0 8px;"
			])
			for s in range(num_slots + 1):
				slot_num = f"<span style='{slot_num_css_style}'>{s}</span>"
				slot_divs_array += f"<div style='{slot_div_css_style}'>{slot_num}</div>"

			slots_div_css_style = "".join([
				"background-color: white;",
				f"border: {border_thickness}px solid black;",
				f"width: {(num_slots + 1) * slot_side}px;",
				f"height: {slot_side}px;",
				"margin: 50px auto 0 auto;"
			])
			html += f"<div style='{slots_div_css_style}'>{slot_divs_array}</div>"

		outer_frame_size = (printing_correction*outer_frame_size).astype(int)
		margin = ((page_size-outer_frame_size)/2).astype(int)
		outer_frame_div_css_style = "".join([
			"float: left;",
			f"background-color: white;",
			f"width: {outer_frame_size[0]}px;",
			f"height: {outer_frame_size[1]}px;",
			f"border-radius: {border_radius}px;",
			f"margin: {margin[1]}px 0 0 {margin[0]}px;"
		])
		html = f"<div style='{outer_frame_div_css_style}'>{html}</div>"

		page_div_css_style = "".join([
			f"background-color: gray;",
			f"width: {page_size[0]}px;",
			f"height: {page_size[1]}px;",
			"float: left;"
		])
		html = f"<div style='{page_div_css_style}'>{html}</div>"
		html = f"<html><head></head><body style='margin: 0; padding: 0;'>{html}</body></html>"
		return html

	@staticmethod
	def render_to_image(slots):
		html = SlotsUtils.render_to_html(slots)
		with suppress_stdout():
			image_bytes = imgkit.from_string(string=html, output_path=False)
		image = Image.open(io.BytesIO(image_bytes))
		return image
