
import io
import json
from collections import OrderedDict
import numpy as np
import imgkit
from pathlib import Path
from PIL import Image
from dnd_cheatsheets.utils import suppress_stdout, printProgressBar

config = dict(
	page_size_px = [2480, 3508],  # A4 format in pixels
	ticket_padding_size = 30,
	num_cols = 2,
	font_size = 42,
	themes = {
		"light": {"fg": "black", "bg": "white"},
		"dark": {"fg": "white", "bg": "black"},
		"sol_light": {"fg": "#303030", "bg": "#f7f0e1"},
		"sol_dark": {"fg": "#f7f0e1", "bg": "#303030"}
	},
	theme = "light"
)


class SpellsUtils:
	@staticmethod
	def init_spells_file(data_dir, character_path):
		with open(character_path, "r") as character_fp:
			character_data = json.load(character_fp)

		spells = []
		for entry in character_data["spells"]:
			spells.append(OrderedDict({
				"visible": True,
				"name": entry["name"],
				"level": entry["level"],
				"casting_time": entry["castingTime"],
				"range": entry["range"],
				"components": entry["components"],
				"duration": entry["duration"],
				"description": entry["description"],
				"short_description": ["x" * 54],
				"at_higher_levels": entry["higherLevels"]
			}))
		level_indices = np.argsort([entry["level"] for entry in spells])
		spells = (np.array(spells)[level_indices]).tolist()

		char_name = character_path.parts[-1].replace("character_", "").replace(".cah", "")
		char_dir = Path(data_dir, char_name)
		if not char_dir.exists():
			char_dir.mkdir()
		else:
			print(f" Stopped initialization, Character exists: {char_dir} ")
			return
		spells_path = Path(char_dir, "spells.json")
		spells_path.write_text(json.dumps(spells, indent=4, sort_keys=False))
		print(f" Spells initialized: {spells_path} ")

		char_data_out = json.dumps(character_data, indent=4, sort_keys=False)
		cha_json_path = Path(char_dir, f"{char_name}.json")
		cha_json_path.write_text(char_data_out)

	@staticmethod
	def optimize_sheet_surface(spells):
		ahl = "at_higher_levels"
		all_bullets = [
			len(entry["short_description"]) + (1 if ahl in entry and entry[ahl] else 0)
			for entry in spells
		]
		sorted_indices = np.argsort(all_bullets)
		sorted_bullets = (np.array(spells)[sorted_indices]).tolist()
		return sorted_bullets

	@staticmethod
	def render_to_html(spells, verbose=False):
		page_size = config["page_size_px"]
		ticket_padding_size = config["ticket_padding_size"]
		num_cols = config["num_cols"]
		font_size = config["font_size"]
		themes = config["themes"]
		theme = config["theme"]

		new_row_cnt = int(num_cols)
		cout, html = "", ""
		for entry in spells:
			level = "<b>{}</b>".format(entry["level"])
			name = "<b>{}</b>".format(entry['name'])
			casting_time = "<b>{}</b>".format(
				entry["casting_time"].replace("1 action", "A") \
					.replace("1 bonus action", "BA").replace("1 reaction", "R") \
					.replace("minute", "min")
			)
			duration = "<b>{}</b>".format(
				entry["duration"].replace("conc,", "CONC") \
				.replace("up to","").replace("Up to","").replace("<=", "") \
				.replace("minute", "min")
				.replace("Concentration", "CONC") \
				.replace("Instantaneous", "insta")
			)
			range_ = "<b>{}</b>".format(entry['range'])
			# components = entry["components"]
			components = "".join([c for c in entry["components"] if c in ["V", "S", "M", ","]])
			short_desc = "".join([f" - {x} </br>" for x in entry["short_description"]])
			ahl = f" - @ hier lvls: {entry['at_higher_levels']}" \
				if "at_higher_levels" in entry and entry['at_higher_levels'] else ""
			ticket = "<span> {}. {} - {} - {} - {} - {}</span> </br> <span>{}</span>{}".format(
				level, name, casting_time, duration, range_, components, short_desc, ahl
			)
			ticket_width = int(page_size[0] / num_cols) - (2 * ticket_padding_size)
			ticket_div_css_style = "".join([
				f"width: {ticket_width}px;",
				f"font-size: {font_size}px;",
				"font-family: Arial, Helvetica, sans-serif;",
				"float: left;",
				f"padding: {ticket_padding_size}px {ticket_padding_size}px 0 {ticket_padding_size}px;"
			])
			new_row_cnt -= 1
			if new_row_cnt == num_cols - 1:
				start_group_div, end_group_div = "<div style='float: left;'>", ""
			elif new_row_cnt == 0:
				start_group_div, end_group_div = "", "</div>"
				new_row_cnt = int(num_cols)
			else:
				start_group_div, end_group_div = "", ""

			html += f"{start_group_div}<div style='{ticket_div_css_style}'>{ticket}</div>{end_group_div}"
			if verbose: cout += "{}\n{}\n".format("-" * 100, ticket)

		page_div_css_style = "".join([
			f"color: {themes[theme]['fg']};",
			f"background-color: {themes[theme]['bg']};",
			f"width: {page_size[0]}px;",
			f"height: {page_size[1]}px;",
			"float: left;"
		])
		html = f"<div style='{page_div_css_style}'>{html}</div>"
		html = f"<html><head></head><body style='margin: 0; padding: 0;'>{html}</body></html>"
		if verbose: print(cout)

		return html

	@staticmethod
	def render_to_image(spells):
		html = SpellsUtils.render_to_html(spells)
		with suppress_stdout():
			image_bytes = imgkit.from_string(string=html, output_path=False)
		image = Image.open(io.BytesIO(image_bytes))
		return image

	@staticmethod
	def load_spells(spells_src_path, optimize_surface):
		page_size = config["page_size_px"]
		with open(spells_src_path, "r") as spells_fp:
			spells = list(json.load(spells_fp))

		invisible_spells = [entry for entry in spells if not entry["visible"]]
		for spell in invisible_spells:
			print(f" Ignoring spell: {spell['name']} ")

		spells = [entry for entry in spells if entry["visible"]]

		if optimize_surface:
			spells = SpellsUtils.optimize_sheet_surface(spells)

		spells_image = SpellsUtils.render_to_image(spells)
		if spells_image.height <= page_size[1]:
			return [spells]
		print(f" Spells can not fit on a single page, splitting into multiple pages...")

		spells_stack = [dict(spell) for spell in spells]
		spell_groups = []
		while len(spells_stack):
			spell_group = []
			while True:
				if len(spells_stack) == 0:
					break
				prog = [len(spells) - len(spells_stack), len(spells)]
				printProgressBar(
					prog[0] + 1, prog[1], prefix=f'Sorting out spell: {prog[0] + 1}/{prog[1]}', length=50
				)
				spell = dict(spells_stack[0])
				spell_group.append(spell)
				spell_group_image = SpellsUtils.render_to_image(spell_group)
				if spell_group_image.height <= page_size[1]:
					spells_stack.pop(0)
				else:
					spell_group.pop(-1)
					break
			spell_groups.append(spell_group)
		print(f"\n -> Number of pages: {len(spell_groups)}")

		return spell_groups