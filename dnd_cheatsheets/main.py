
import os
import re
from pathlib import Path
import argparse
from dnd_cheatsheets.spells_utils import SpellsUtils
from dnd_cheatsheets.slots_utils import SlotsUtils

# cwd: shows the real current working directory (of the __file__)
# stays invariant to e.g. soft linking, and always points to where this
# script is and not where it's being called from.
cwd = Path(os.path.dirname(os.path.realpath(__file__)))


def parse_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument(
		'--char', help="Enter character filename (.cah)", required=True
	)
	parser.add_argument(
		'--init', help="Init action", action='store_true',
		default=False, required=False
	)
	parser.add_argument(
		'--gen-cs', help="Generate CheatSheet", action='store_true',
		default=False, required=False
	)
	parser.add_argument(
		'--opt-surf', help="Optimize CheatSheet Surface", action='store_true',
		default=False, required=False
	)
	parser.add_argument(
		'--gen-ss', help="Generate Slots Sliders", action='store_true',
		default=False, required=False
	)
	args = dict(vars(parser.parse_args()))
	if args["init"]: args["gen_cs"], args["gen_ss"] = False, False
	if args["gen_cs"]: args["gen_ss"] = False
	return args


def create_spells_cheatsheet(character_dir, optimize_surface):
	print("\n----- Creating Spells Cheatsheet -----")
	spells_src_path = Path(character_dir, "spells.json")
	if not spells_src_path.exists():
		print(f" Can not find: {spells_src_path} ")
		return
	print("\n".join([
		" Reading in: {} ".format(spells_src_path),
		" Sheet Surface Optimization: {} ".format("ON" if optimize_surface else "OFF")
	]))
	spell_groups = SpellsUtils.load_spells(spells_src_path, optimize_surface)

	for i, spells in enumerate(spell_groups):
		print(" Generating image... ")
		image = SpellsUtils.render_to_image(spells)
		num = f"_{i + 1}" if len(spell_groups) > 1 else ""
		spells_image_path = Path(character_dir, f"spells{num}.png")
		image.save(str(spells_image_path))
		print(f" Wrote: {spells_image_path} ")


def create_slots_sliders(character_dir):
	print("\n----- Creating Slots sliders -----")
	slots_src_path = Path(character_dir, "slots.json")
	if not slots_src_path.exists():
		print(f" Can not find: {slots_src_path} ")
		return
	print(" Reading in: {} ".format(slots_src_path))
	slots = SlotsUtils.load_slots(slots_src_path)
	print(" Generating image... ")
	image = SlotsUtils.render_to_image(slots)
	slots_image_path = Path(character_dir, "slots.png")
	image.save(str(slots_image_path))
	print(f" Wrote: {slots_image_path} ")


def main(args):
	data_dir = Path(cwd, "../data").resolve()
	if not data_dir.exists():
		print(f" data dir not found, exiting... ")
		return

	match = re.findall(r"^character_([\w_]+)[.]cah$", args["char"], re.IGNORECASE)
	if len(match) == 0:
		print(f" Invalid character filename: {args['char']} ")
		return
	character_path, char_name = Path(data_dir, args['char']), match[0]
	if not character_path.exists():
		print(f" Character file does not exist: {character_path} ")
		return
	character_dir = Path(data_dir, char_name)

	if args["init"]:
		SpellsUtils.init_spells_file(data_dir, character_path)
	elif args["gen_cs"]:
		if not character_dir.exists():
			print(f" Character not initialized - can not find: {character_dir} ")
			return
		create_spells_cheatsheet(character_dir, optimize_surface=args["opt_surf"])
	elif args["gen_ss"]:
		if not character_dir.exists():
			print(f" Character not initialized - can not find: {character_dir} ")
			return
		create_slots_sliders(character_dir)
	else:
		print(" No action given, exiting... ")


if __name__=="__main__":
	main(args=parse_arguments())