# D&D cheat-sheets generator

Place your character files (`.cah`) into `data/` and run `--init`. You will get character-specific subdirectory inside of `data/`. Then run `--gen-cs` to get spells cheat-sheet.

```bash
# --- docker build:
docker build -t dnd_cheatsheets .

# --- docker run (examples):
# init character directory (based on .cah file)
docker run \
    -v /home/vidzor/Projects/dnd_cheatsheets/data:/home/dnd/dnd_cheatsheets/data -it \
    dnd_cheatsheets python3 -m dnd_cheatsheets.main --char character_Bajonee.cah --init
# generate spells cheatsheet (as image inside character directory)
docker run \
    -v /home/vidzor/Projects/dnd_cheatsheets/data:/home/dnd/dnd_cheatsheets/data -it \
    dnd_cheatsheets python3 -m dnd_cheatsheets.main --char character_Bajonee.cah --gen-cs
# generate slots image
docker run \
    -v /home/vidzor/Projects/dnd_cheatsheets/data:/home/dnd/dnd_cheatsheets/data -it \
    dnd_cheatsheets python3 -m dnd_cheatsheets.main --char character_Bajonee.cah --gen-ss
```


