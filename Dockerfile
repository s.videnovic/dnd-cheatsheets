FROM python:3.8.10

#ENV PYTHONDONTWRITEBYTECODE 1
#ENV PYTHONUNBUFFERED 1
#ENV PIP_DISABLE_PIP_VERSION_CHECK=on
ARG POETRY_VERSION=1.1.13
ARG home_dir=/home/dnd/
ARG project_name=dnd_cheatsheets

RUN apt -y update --fix-missing && \
    apt -y install wkhtmltopdf && \
    apt -y update && \
    apt install -f

# Copy the font to the appropriate location.
# The font is in a ttf in the same directory as the Dockerfile.
RUN mkdir -p /usr/share/fonts/truetype/noto
COPY arial.ttf /usr/share/fonts/truetype/noto
RUN chmod 644 /usr/share/fonts/truetype/noto/*
# Rebuild the font cache.
RUN fc-cache -fv

# set display port to avoid crash
ENV DISPLAY=:99

RUN mkdir ${home_dir}
WORKDIR ${home_dir}

RUN pip install "poetry==${POETRY_VERSION}"
RUN poetry config virtualenvs.create false
RUN poetry new ${project_name}
WORKDIR ${home_dir}${project_name}/

COPY poetry.lock pyproject.toml ./
ADD ${project_name} ./${project_name}/
RUN mkdir ./data/

RUN poetry install

